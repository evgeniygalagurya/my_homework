package org.danit;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// http://192.168.1.107:24/history
@RequiredArgsConstructor
public class HistoryServlet extends HttpServlet {

  private final History history;

  @SneakyThrows
  private void redirect(HttpServletResponse resp, String to) {
    resp.sendRedirect(to);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    try (PrintWriter w = resp.getWriter()) {
      Auth.tryGetCookie(req)
              .ifPresentOrElse(
                      uid -> history.getAll(uid).forEach(w::println),
                      () -> redirect(resp, "/")
//              () -> w.write("User not registered")
              );
    }
  }

}
