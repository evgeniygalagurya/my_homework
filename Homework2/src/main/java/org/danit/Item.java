package org.danit;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

//public record Item(int x, int y, int z, LocalDateTime at) {
//
//  public static Item of(int x, int y, int z) {
//    return new Item(x, y, z, LocalDateTime.now());
//  }
public record Item(int x, int y, int z, Timestamp at) {

  public static Item of(int x, int y, int z) {
    return new Item(x, y, z, new Timestamp(System.currentTimeMillis()));
  }

  public static Item of(int x, int y, int z, Timestamp at) {
    return new Item(x, y, z, at);
  }

  @Override
  public String toString() {
    return String.format("%d + %d = %d, at = %s", x, y, z, at);
  }

}
