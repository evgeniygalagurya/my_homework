package org.danit;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import sql.DbConn;

import javax.servlet.MultipartConfigElement;
import java.net.InetSocketAddress;

public class Launcher {

  public static void main(String[] args) throws Exception {
//    Server server = new Server(8080);
    InetSocketAddress inetSocketAddress = new InetSocketAddress("192.168.1.107", 24);
    Server server = new Server(inetSocketAddress);

    History history =
            new HistoryInMemory();
//        new HistoryInDb(DbConn.make());
    ServletContextHandler handler = new ServletContextHandler();

    String osStaticLocation = ResourcesOps.dirUnsafe("static-content");
    handler.addServlet(new ServletHolder(new StaticContentServlet(osStaticLocation)), "/static/*");
    handler.addServlet(new ServletHolder(new LoginServlet()), "/login");
    handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");
    handler.addServlet(new ServletHolder(new HandleFormServlet()), "/form");
    handler.addServlet(new ServletHolder(new FileDownloadServlet()), "/dl");

    ServletHolder sh = new ServletHolder(new FileUploadServlet());
    // upload only
    sh.getRegistration().setMultipartConfig(new MultipartConfigElement("./from-user"));
    handler.addServlet(sh, "/upload");

    handler.addServlet(new ServletHolder(new CalculatorServlet(history)), "/calc");
    handler.addServlet(new ServletHolder(new HistoryServlet(history)), "/history");
    handler.addServlet(new ServletHolder(new SumServlet1()), "/sum1");
    handler.addServlet(new ServletHolder(new SumServlet2()), "/sum2");
    handler.addServlet(new ServletHolder(new SumServlet3()), "/sum3");
    handler.addServlet(new ServletHolder(new HtmlPageServlet()), "/html");
    handler.addServlet(new ServletHolder(new BinaryContentServlet()), "/bin");
    handler.addServlet(new ServletHolder(new TemplateServlet()), "/template");
//    handler.addServlet(new ServletHolder(new HelloServlet("Hello from Java")), "/");
//    handler.addServlet(HelloServlet.class, "/");
//    handler.addServlet("org.danit.HelloServlet", "/");

    server.setHandler(handler);

    server.start();
    server.join();
  }

}