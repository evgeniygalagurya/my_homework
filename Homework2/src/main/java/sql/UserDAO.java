package sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public class UserDAO implements DAO<User> {

  private final Connection conn;

  public UserDAO(Connection conn) {
    this.conn = conn;
  }

  private void insert(User students) throws Exception{
    PreparedStatement stmt = conn.prepareStatement("INSERT INTO students (name) values (?)");
    stmt.setString(1, students.name());
    stmt.execute();
  }

  private void update(User students) throws Exception{
    PreparedStatement stmt = conn.prepareStatement("UPDATE students SET name = ? WHERE id = ?");
    stmt.setString(1, students.name());
    stmt.setInt(2, students.id());
    stmt.execute();
  }

  @Override
  public void save(User students) throws Exception {
    if (students.id() == null) insert(students);
    else                   update(students);
  }

  @Override
  public Optional<User> load(int id) throws Exception {
    PreparedStatement stmt = conn.prepareStatement("SELECT id, name FROM students WHERE id = ?");
    stmt.setInt(1, id);
    // 0 or 1 record
    ResultSet rs = stmt.executeQuery();
    if (rs.next()) {
      return Optional.of(new User(
          rs.getInt("id"),
          rs.getString("name")
      ));
    }
    return Optional.empty();
  }

  @Override
  public void delete(int id) throws Exception {
    PreparedStatement stmt = conn.prepareStatement("DELETE FROM students WHERE id = ?");
    stmt.setInt(1, id);
    stmt.executeUpdate();
  }
}
