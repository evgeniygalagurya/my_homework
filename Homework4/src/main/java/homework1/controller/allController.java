package homework1.controller;

import homework1.Customer;
import homework1.custom.TableHeader;
import homework1.forms.InputForm;
import homework1.service.allService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
@Log4j2
@Controller
@RequiredArgsConstructor
public class allController {

    private final allService service;

     @GetMapping("customer-create")
    public String formShow() {
        return "customer-create";
    }
    @PostMapping("customer-create")
    @ResponseBody
    public String formProcess(InputForm form, HttpServletRequest rq) {

        service.createCustomer(form.getName(), form.getEmail(), Integer.valueOf(form.getAge()));

        return "Customer create";
    }
    private Map<String, Object> data() {
        TableHeader th = new TableHeader("Id", "Name", "E-mail", "Account");
        List<Customer> tb = service.allCustomers();
        return Map.of(
                "thead", th,
                "tbody", tb
        );
    }
    @GetMapping("list")
    public String listCustomer(Model model) {
        data().forEach(model::addAttribute);
        return "list-customers";
    }
    @GetMapping("account-create")
    public String accountCreate() {
        return "account-create";
    }
    @PostMapping("account-create")
    @ResponseBody
    public String accountCreate(InputForm form, HttpServletRequest rq) {
        service.accountCreate((form.getIdCustomer() - 1), form.getInputCurrency());
        return "Account create";
    }
    @GetMapping("customer")
    public String customer() {
        return "customer";
    }
    @PostMapping("customer")
    @ResponseBody
    public String customer(InputForm form, HttpServletRequest rq) {
        return String.valueOf(service.customer((form.getIdCustomer() - 1)));
    }
    @GetMapping("customer-delete")
    public String customerDelete() {
        return "customer-delete";
    }
    @PostMapping("customer-delete")
    @ResponseBody
    public String customerDelete(InputForm form, HttpServletRequest rq) {
        service.customerDelete((form.getIdCustomer() - 1));
        return "Customer delete";
    }
    @GetMapping("customer-refactor")
    public String customerRefactor() {
        return "customer-refactor";
    }
    @PostMapping("customer-refactor")
    @ResponseBody
    public String customerRefactor(InputForm form, HttpServletRequest rq) {
        Customer customer =  service.customer(form.getIdCustomer() - 1) ;
        service.customerRefactor(customer, form.getName(),form.getEmail(),Integer.parseInt(form.getAge()));
        return "customer is refactor";
    }
    @GetMapping("account-delete")
    public String accountDelete() {
        return "account-delete";
    }
    @PostMapping("account-delete")
    @ResponseBody
    public String accountDelete(InputForm form, HttpServletRequest rq) {
        service.accountDelete((form.getIdCustomer() - 1), (form.getIdAccount() - 1));
        return "Account delete";
    }
    @GetMapping("refill")
    public String moneyPut() {
        return "refill";
    }
    @PostMapping("refill")
    @ResponseBody
    public String moneyPut(InputForm form, HttpServletRequest rq) {
        service.refillAccount(form.getIdCustomer() - 1, form.getIdAccount() - 1, form.getValueM());
        return "Money put";
    }
    @GetMapping("withdraw")
    public String moneyGet() {
        return "withdraw";
    }
    @PostMapping("withdraw")
    @ResponseBody
    public String moneyGet(InputForm form, HttpServletRequest rq) {
        service.withdrawAccount(form.getIdCustomer() - 1, form.getIdAccount() - 1, form.getValueM());
        return "Money get";
    }

    @GetMapping("transfer")
    public String moneyTransfer() {
        return "transfer";
    }
    @PostMapping("transfer")
    @ResponseBody
    public String moneyTransfer(InputForm form, HttpServletRequest rq) {
        service.transfer(form.getIdCustomer() - 1, form.getIdAccount() - 1, form.getValueM(), form.getIdCustomer2() - 1, form.getIdAccount2() - 1);
        return "Money transfer";
    }

}
