package homework1;

import homework1.DAO.Identifable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.UUID;

@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor

public class Account implements Identifable {
    private Long id;
    private String number = RandomNumber.randomDiceNumber();
    private Currency currency;
    private Double balance = (double) 0;
    private Customer customer;
    public Account(Currency currency, Customer customer) {
        this.currency = currency;
        this.customer = customer;
    }
    public Account(Currency currency ) {
        this.currency = currency;
    }
    @Override
    public String toString() {
//        return "Account {id = " + id + ", number = " + number + ", currency = " + currency + ", balance = " + balance +
//                ", customer =" + customer + "}";
        return "{number = " + number + ", currency = " + currency + ", balance = " + balance + "}"; }

    @Override
    public int id() {
        return 0;
    }
}
