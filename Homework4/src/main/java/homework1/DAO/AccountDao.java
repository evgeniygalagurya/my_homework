package homework1.DAO;

import homework1.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountDao implements DAO <Account> {

    public List<Account> listAccount = new ArrayList<>();

    public AccountDao() { this.listAccount = listAccount; }

    @Override
    public Account save(Account account) {
        listAccount.add((Account) account);
        return account;
    }
    @Override
    public boolean delete(Account account) {
        return listAccount.remove(account);
    }
    @Override
    public void deleteAll(List entities) {
        listAccount.clear();
    }
    @Override
    public void saveAll(List entities) {
        this.listAccount = new ArrayList<>();
    }
    @Override
    public List<Account> findAll() {
        return listAccount;
    }
    @Override
    public boolean deleteById(int id) {
        listAccount.remove(id);
        return true;
    }

    public Account getOne(int id){
       return listAccount.get(id);
       }
    }



