package homework1.DAO;

import homework1.Customer;

import java.util.ArrayList;
import java.util.List;
public class CustomerDao implements DAO <Customer> {

    public List<Customer> listCustomers = new ArrayList<>();

    @Override
    public Customer save(Customer customer) {
        listCustomers.add(customer);
        return customer;
    }
    @Override
    public boolean delete(Customer customer) {
        return listCustomers.remove(customer);
    }
    @Override
    public void deleteAll(List entities) {
        listCustomers.clear();
    }
    @Override
    public void saveAll(List entities) {
//        this.customer  = customer ;
    }
    @Override
    public List<Customer> findAll() {
        return listCustomers;
    }
    @Override
    public boolean deleteById(int id) {
        listCustomers.remove(id);
        return false;
    }
    @Override
    public Customer getOne(int id) {
        return listCustomers.get(id);
        }
    }


