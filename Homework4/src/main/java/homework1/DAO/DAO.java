package homework1.DAO;

import homework1.Account;
import homework1.Customer;

import java.util.List;

public interface DAO <T extends Identifable>{
    T save(T obj);
    boolean delete(T obj);
    void deleteAll(List<T> entities);
    void saveAll(List<T> entities);
    List<T> findAll();
    boolean deleteById(int id);
    T getOne(int id);
}
