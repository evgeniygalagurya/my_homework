package homework1;

import homework1.DAO.AccountDao;
import homework1.DAO.Identifable;
import lombok.*;

import java.util.List;
@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@Data

public class Customer implements Identifable {
    AccountDao accountDao = new AccountDao();
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<Account> listAccount = accountDao.findAll();

    public Customer(Long id, String name, String email, Integer age) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.age = age;
    }
    public void addAccounts(Account account) {
        accountDao.save(account);
    }
    public void delAccounts(int id) {
        accountDao.deleteById(id);
    }
    @Override
    public String toString() {
        return "Customer {id = " + id + ", name = " + name + ", email = " + email + ", age = " + age +
                ", accounts =" + listAccount + "}";
    }
    public Account getAccountList(int id){
        return listAccount.get(id);
    }
    @Override
    public int id() {
        return 0;
    }

}
