package homework1.forms;
import homework1.Currency;
import lombok.Data;
@Data
public class InputForm {
  String name;
  String email;
  String age;
  Double valueM;

  int idCustomer;
  int idAccount;
  int idCustomer2;
  int idAccount2;
  Currency inputCurrency;
  int checkPage;
  int id;
  }
