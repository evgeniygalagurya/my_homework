package homework1.forms;
import lombok.Data;
@Data
public class LoginForm {
  String name;
  String email;
  Integer age;
}
