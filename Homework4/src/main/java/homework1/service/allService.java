package homework1.service;

import homework1.Account;
import homework1.Currency;
import homework1.Customer;
import homework1.DAO.CustomerDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class allService {

    CustomerDao customerDao = new CustomerDao();

    public void createCustomer(String name, String email, Integer age) {
        customerDao.save(new Customer((long) customerDao.listCustomers.size() + 1, name, email, age));
    }
    public void accountCreate(int id, Currency currency) {
        customerDao.getOne(id).addAccounts(new Account(currency));
    }
    public List<Customer> allCustomers() {
        return customerDao.findAll();
    }
    public Customer customer(int id) {
        return customerDao.getOne(id);
    }
    public void customerDelete(int id) {
        customerDao.deleteById(id);
    }
    public void  customerRefactor(Customer customer, String name, String email, int age) {
        customer.setEmail(email);
        customer.setAge(age);
        customer.setName(name);
    }
    public void accountDelete(int id, int ida) {
        customerDao.getOne(id).delAccounts(ida);
    }
    public void refillAccount(int id, int ida, double valueM) {
        customerDao.listCustomers.get(id).getAccountList(ida).setBalance(valueM);
    }

    public void withdrawAccount(int id, int ida, double valueM) {
        customerDao.listCustomers.get(id).getAccountList(ida).setBalance( customerDao.listCustomers.get(id).getAccountList(ida).getBalance() - valueM);
    }

    public void transfer(int id, int ida, double valueM, int id2, int ida2) {
        customerDao.listCustomers.get(id).getAccountList(ida).setBalance(customerDao.listCustomers.get(id).getAccountList(ida).getBalance() - valueM);
        customerDao.listCustomers.get(id2).getAccountList(ida2).setBalance(customerDao.listCustomers.get(id2).getAccountList(ida2).getBalance() + valueM);
    }
}
