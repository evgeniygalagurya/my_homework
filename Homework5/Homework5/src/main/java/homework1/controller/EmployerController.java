package homework1.controller;

import homework1.dto.EmployerRequest;
import homework1.dto.EmployerResponse;
import homework1.models.Employer;
import homework1.models.InputItems;
import homework1.service.EmployerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Log4j2
@Controller
@RequiredArgsConstructor
public class EmployerController {

    private final EmployerService employerService;
    private final EntityManager em;
    @GetMapping("employer")
    @ResponseBody
    public List<EmployerResponse> handleGet() {
        return employerService.findAll();
    }
    @GetMapping("employer/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        employerService.delete(id);
        return "Employer deleted";
    }
    @PostMapping("employer/create")
    @ResponseBody
    public void create(@RequestBody EmployerRequest p) {
        employerService.create(p);
    }
    @GetMapping("employer/show/{id}")
    @ResponseBody
    public List<EmployerResponse> show(@PathVariable("id") Long id) {
        return employerService.show(id);
    }
    @GetMapping("employer/delete-all")
    @ResponseBody
    public String deleteAll() {
        employerService.deleteAll();
        return "All employer deleted";
    }

    @GetMapping("employer-create")
    public String employerCreate(EmployerRequest employerRequest) {
        return "employer-create";
    }

    @PostMapping("employer-create")
    public String formValidate(InputItems form, HttpServletRequest rq, @Valid EmployerRequest employerRequest, BindingResult result) {
//        System.out.println(personForm);
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            fieldErrors.forEach(System.out::println);
            return "employer-create";
        }
        Employer employer = new Employer(form.getName(), form.getAddress());
        employerService.employerCreate(employer);
        return "redirect:navigation";
    }

//    @PostMapping("employer-create")
//    public String employerCreate(InputItems form, HttpServletRequest rq) {
////        Map<String, String[]> allParams = rq.getParameterMap();
//        Employer employer = new Employer(form.getName(), form.getAddress());
//        employerService.employerCreate(employer);
////        log.info("Massege :{} ", "33");
//        return "redirect:navigation";
//    }
}

