package homework1;

import homework1.controller.CustomerController;
import homework1.models.InputItems;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@Log4j2
public class Navigation {

    public static String navigationNumber(InputItems form){
        if(form.getCheckPage() == 0) return "redirect:employer-create";
        if(form.getCheckPage() == 1) return "redirect:customer-create";
        if(form.getCheckPage() == 2) return "redirect:account-create";
        if(form.getCheckPage() == 3) return "redirect:refill";
        if(form.getCheckPage() == 4) return "redirect:withdraw";
        if(form.getCheckPage() == 5) return "redirect:transfer";
        if(form.getCheckPage() == 6) return "redirect:delete-customer";
        if(form.getCheckPage() == 7) return "redirect:delete-account";
        if(form.getCheckPage() == 8) return "redirect:customer";
        if(form.getCheckPage() == 9) return "redirect:delete-employer";


        return "redirect:navigation";

    }
    @GetMapping("navigation")
    public String moneyPut() {
        return "navigation";
    }

    @PostMapping("navigation")

    public String navigation(InputItems form, HttpServletRequest rq) {
        Map<String, String[]> allParams = rq.getParameterMap();
        return navigationNumber(form);
    }
}
