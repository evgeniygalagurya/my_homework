package homework1.repo;

import homework1.models.Account;
import homework1.models.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerRepo extends JpaRepository <Employer, Long> {
}
