-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Хост: jg507082.mysql.ukraine.com.ua:3306
-- Час створення: Сер 04 2023 р., 05:59
-- Версія сервера: 5.7.42-46-log
-- Версія PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `jg507082_evgeniy`
--

-- --------------------------------------------------------

--
-- Структура таблиці `account`
--

CREATE TABLE `account` (
  `id` bigint(20) NOT NULL,
  `balance` double DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `account`
--

INSERT INTO `account` (`id`, `balance`, `currency`, `number`, `customer_id`) VALUES
(1, 200, 'USD', '7606 6131 9483 3846 ', 1),
(2, 400, 'EUR', '7544 3386 5018 4864 ', 2),
(3, 300, 'UAH', '4435 7525 4302 9341 ', 3);

-- --------------------------------------------------------

--
-- Структура таблиці `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `customer`
--

INSERT INTO `customer` (`id`, `age`, `email`, `name`) VALUES
(1, 40, 'evgeniygalagurya@gmail.com', 'Євгеній'),
(2, 32, 'galagurya@rambler.ru', 'Halahuria Yevhenii'),
(3, 41, 'helensvetlya4ok@gmail.com', 'Jimm');

-- --------------------------------------------------------

--
-- Структура таблиці `employee_customer`
--

CREATE TABLE `employee_customer` (
  `employee_id` bigint(20) NOT NULL,
  `customer_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `employee_customer`
--

INSERT INTO `employee_customer` (`employee_id`, `customer_id`) VALUES
(1, 1),
(2, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Структура таблиці `employer`
--

CREATE TABLE `employer` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `employer`
--

INSERT INTO `employer` (`id`, `address`, `name`) VALUES
(1, 'Kharkiv', 'Omega'),
(2, 'Kyiv', 'POP');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKnnwpo0lfq4xai1rs6887sx02k` (`customer_id`);

--
-- Індекси таблиці `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `employee_customer`
--
ALTER TABLE `employee_customer`
  ADD PRIMARY KEY (`employee_id`,`customer_id`),
  ADD KEY `FKpkvqxd5v4uiwvn43dcgkkoaon` (`customer_id`);

--
-- Індекси таблиці `employer`
--
ALTER TABLE `employer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `account`
--
ALTER TABLE `account`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблиці `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблиці `employer`
--
ALTER TABLE `employer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `FKnnwpo0lfq4xai1rs6887sx02k` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `employee_customer`
--
ALTER TABLE `employee_customer`
  ADD CONSTRAINT `FKmfo76h0b2ohhqolu6ro91tyom` FOREIGN KEY (`employee_id`) REFERENCES `employer` (`id`),
  ADD CONSTRAINT `FKpkvqxd5v4uiwvn43dcgkkoaon` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
