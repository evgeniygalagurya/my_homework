package homework1.models;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name="Customer")

public class Customer extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private String password;
    private String phoneNumber;

    @OneToMany(cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch =FetchType.EAGER ,mappedBy = "customer")
    private List <Account> accounts;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "customers",cascade = {CascadeType.REMOVE})
    private Set<Employer> employers = new HashSet<>();

    public Customer() {
    }
    public Customer(String name, Integer age, String email, String phoneNumber) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }
    public void addAccounts(Account account) {
        account.setCustomer(this);
        accounts.add(account);
    }

}
