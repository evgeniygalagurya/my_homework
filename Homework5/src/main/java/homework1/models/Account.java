package homework1.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import homework1.custom.RandomNumber;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name="Account")

public class Account extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String number = RandomNumber.randomDiceNumber();

    @Enumerated(EnumType.STRING)
    private Currency currency;

    private Double balance = (double) 0;

    @CreationTimestamp
    private LocalDateTime createdDateTime;

    @JsonIgnore
    @ManyToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER )
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public Account() {
    }

    public Account(Currency currency ) {
        this.currency = currency;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "{number = " + number + ", currency = " + currency + ", balance = " + balance + "}"; }

}
