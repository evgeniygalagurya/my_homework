package homework1.service;

import homework1.dto.CustomerMapper;
import homework1.dto.CustomerRequest;
import homework1.dto.CustomerResponseDto;
import homework1.models.Customer;
import homework1.repo.CustomerRepo;
import homework1.repo.EmployerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

  private final CustomerRepo customerRepo;
  private final EmployerRepo employerRepo;
  private final CustomerMapper dtoMapper;


  public List<CustomerResponseDto> showAll() {
    return customerRepo.findAll()
        .stream()
        .map(p -> {
            CustomerResponseDto pp = new CustomerResponseDto() {{
            setId(p.getId());
            setName(p.getName());
            setAge(p.getAge());
            setEmail(p.getEmail());
            setPhoneNumber(p.getPhoneNumber());
            setAccounts(p.getAccounts());
            setEmployers(p.getEmployers());
            }};
          return pp;
        })
        .filter(p -> p.getName() != null)
        .filter(p -> !p.getName().isBlank())
        .toList();
  }
    public List<Customer> showAll1(Integer  page, Integer size){
        Pageable pageable = PageRequest.of(page,size);
        Page<Customer> customerPage = customerRepo.findAll(pageable);
        return customerPage.toList();
    }

    public void delete(Long id) {
        customerRepo.deleteById(id);
    }

    // Работает только с @RestController
    public void create(CustomerRequest cc) {
        Customer customer = new Customer();
        customer.setName(cc.getName());
        customer.setEmail(cc.getEmail());
        customer.setAge(cc.getAge());
        customerRepo.save(customer);
    }
    public void create1(Customer customer, Long id) {
        employerRepo.getReferenceById(id).addCustomer(customer);
        customerRepo.save(customer);
    }

    public List<CustomerResponseDto> show(Long id) {
        return customerRepo.findById(id)
                .stream()
                .map(p -> {
                    CustomerResponseDto pp = new CustomerResponseDto() {{
                        setId(p.getId());
                        setName(p.getName());
                        setAge(p.getAge());
                        setEmail(p.getEmail());
                        setPhoneNumber(p.getPhoneNumber());
                    }};
                    return pp;
                })
                .filter(p -> p.getName() != null)
                .filter(p -> !p.getName().isBlank())
                .toList();
    }

    public void deleteAll() {
      customerRepo.deleteAll();
    }
}
