package homework1.service;

import homework1.dto.CustomerMapper;
import homework1.dto.CustomerRequest;
import homework1.dto.CustomerResponseDto;
import homework1.models.Customer;
import homework1.models.Employer;
import homework1.repo.CustomerRepo;
import homework1.repo.EmployerRepo;
import homework1.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CustomerServiceTest {

    @Mock
    private CustomerRepo customerRepo;

    @Mock
    private EmployerRepo employerRepo;

    @Mock
    private CustomerMapper dtoMapper;

    @InjectMocks
    private CustomerService customerService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testShowAll() {
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("Customer 1", 25, "email@example.com", "+380679384346", "123"));
        customers.add(new Customer("Customer 2", 30, "another@example.com", "+380679384347", "345"));

        when(customerRepo.findAll()).thenReturn(customers);

        List<CustomerResponseDto> responses = customerService.showAll();

        assertEquals(2, responses.size());
        assertEquals("Customer 1", responses.get(0).getName());
        assertEquals("Customer 2", responses.get(1).getName());

        verify(customerRepo, times(1)).findAll();
    }

    @Test
    public void testShowAll1() {
        int page = 0;
        int size = 10;
        Pageable pageable = Pageable.ofSize(size);

        Page<Customer> customerPage = mock(Page.class);
        List<Customer> customerList = new ArrayList<>();
        when(customerPage.toList()).thenReturn(customerList);
        when(customerRepo.findAll(pageable)).thenReturn(customerPage);

        List<Customer> result = customerService.showAll1(page, size);

        assertEquals(customerList, result);
        verify(customerRepo, times(1)).findAll(pageable);
    }
    @Test
    public void testDelete() {
        Long customerId = 1L;
        customerService.delete(customerId);

        verify(customerRepo, times(1)).deleteById(customerId);
    }

    @Test
    public void testCreate() {
        CustomerRequest request = new CustomerRequest("Ivan", 25, "ivan@gmail.com");
        Customer customer = new Customer();
        customer.setName(request.getName());
        customer.setAge(request.getAge());
        customer.setEmail(request.getEmail());

        customerService.create(request);

//        verify(customerRepo, times(1)).save(customer);
    }

    @Test
    public void testCreate1() {
        Long employerId = 1L;
        Customer customer = new Customer("Customer 1", 25, "email@example.com", "+380679384346", "123");
        Employer employer = new Employer();

        when(employerRepo.getReferenceById(employerId)).thenReturn(employer);

        customerService.create1(customer, employerId);

//        verify(employer, times(1)).addCustomer(customer);
        verify(customerRepo, times(1)).save(customer);
    }

    @Test
    public void testShow() {
        Long customerId = 1L;
        Customer customer = new Customer("Customer 1", 25, "email@example.com", "+380679384346", "123");
        when(customerRepo.findById(customerId)).thenReturn(Optional.of(customer));

        List<CustomerResponseDto> responses = customerService.show(customerId);

        assertEquals(1, responses.size());
        assertEquals("Customer 1", responses.get(0).getName());

        verify(customerRepo, times(1)).findById(customerId);
    }

    @Test
    public void testDeleteAll() {
        customerService.deleteAll();

        verify(customerRepo, times(1)).deleteAll();
    }

}