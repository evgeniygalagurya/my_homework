package homework1.service;

import homework1.custom.RandomNumber;
import homework1.models.Currency;
import homework1.models.Customer;
import homework1.repo.AccountRepo;
import homework1.repo.CustomerRepo;
import homework1.dto.AccountRequest;
import homework1.models.Account;
import homework1.dto.AccountResponseDto;
import homework1.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class AccountServiceTest {

    @Mock
    private AccountRepo accountRepo;

    @Mock
    private CustomerRepo customerRepo;

    @InjectMocks
    private AccountService accountService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindAll() {
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account(Currency.EUR));
        accounts.add(new Account(Currency.UAH));

        assertEquals(2, accounts.size());
        assertEquals(Currency.EUR, accounts.get(0).getCurrency());
        assertEquals(Currency.UAH, accounts.get(1).getCurrency());
    }
    @Test
    public void testDelete() {
        Long accountId = 1L;
        accountService.delete(accountId);

        verify(accountRepo, times(1)).deleteById(accountId);
    }

    @Test
    public void testCreate() {
        AccountRequest request = new AccountRequest();
        Account account = new Account();
        account.setNumber(request.getNumber());
        account.setCurrency(request.getCurrency());
        account.setBalance(request.getBalance());

        accountService.create(request);

        verify(accountRepo, times(1)).save(account);
    }

    @Test
    public void testShow() {
        Long accountId = 1L;
        Account account = new Account(Currency.EUR);
        when(accountRepo.findById(accountId)).thenReturn(Optional.of(account));

        List<AccountResponseDto> responses = accountService.show(accountId);

        assertEquals(1, responses.size());
        assertEquals(Currency.EUR, responses.get(0).getCurrency());

        verify(accountRepo, times(1)).findById(accountId);
    }

    @Test
    public void testDeleteAll() {
        accountService.deleteAll();

        verify(accountRepo, times(1)).deleteAll();
    }

//    @Test
//    public void testAccountCreate() {
//        Long customerId = 1L;
////        AccountRequest request = new AccountRequest();
//        Account account = new Account();
//        List <Account> accounts = new ArrayList<>();
////        account.setNumber("12345641");
//        account.setCurrency(Currency.UAH);
////        account.setBalance(100.0);
//        accountRepo.save(account);
//        Customer customer = new Customer();
//        when(customerRepo.getReferenceById(customerId)).thenReturn(customer);
//        System.out.println(account);
//
//        accountService.accountCreate(customerId, account);
//
//        verify(customer, times(1)).addAccounts(account);
//        verify(accountRepo, times(1)).save(account);
//    }

    @Test
    public void testGetAccountById() {
        Long accountId = 1L;
        Account account = new Account(Currency.EUR);
        when(accountRepo.getReferenceById(accountId)).thenReturn(account);

        Account result = accountService.getAccountById(accountId.intValue());

        assertEquals(account, result);
    }

    @Test
    public void testRefillAccount() {
        Account account = new Account(Currency.EUR);
        double valueToAdd = 50.0;

        accountService.refillAccount(account, valueToAdd);

        assertEquals(50.0, account.getBalance());
        verify(accountRepo, times(1)).save(account);
    }

    @Test
    public void testWithdrawAccount() {
        Account account = new Account(Currency.EUR);
        double valueToWithdraw = 30.0;

        accountService.withdrawAccount(account, valueToWithdraw);

        assertEquals(-30.0, account.getBalance());
        verify(accountRepo, times(1)).save(account);
    }

    @Test
    public void testTransfer() {
        Account sourceAccount = new Account(Currency.EUR);
        Account targetAccount = new Account(Currency.EUR);
        double transferAmount = 50.0;

        accountService.transfer(sourceAccount, transferAmount, targetAccount);

        assertEquals(-50.0, sourceAccount.getBalance());
        assertEquals(50.0, targetAccount.getBalance());

        verify(accountRepo, times(2)).save(any(Account.class));
    }


}






