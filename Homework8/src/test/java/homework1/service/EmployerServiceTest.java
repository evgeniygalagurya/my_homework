package homework1.service;


import homework1.dto.EmployerRequest;
import homework1.dto.EmployerResponse;
import homework1.models.Employer;
import homework1.repo.EmployerRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class EmployerServiceTest {

    @Mock
    private EmployerRepo employerRepo;

    @InjectMocks
    private EmployerService employerService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    public void testFindAll() {
        List<Employer> employers = new ArrayList<>();
        employers.add(new Employer("Employer 1", "Address 1"));
        employers.add(new Employer("Employer 2", "Address 2"));

        assertEquals(2, employers.size());
        assertEquals("Employer 1", employers.get(0).getName());
        assertEquals("Employer 2", employers.get(1).getName());
    }

    @Test
    public void testDelete() {
        Long employerId = 1L;
        employerService.delete(employerId);

        verify(employerRepo, times(1)).deleteById(employerId);
    }

    @Test
    public void testCreate() {
        EmployerRequest request = new EmployerRequest("New Employer", "New Address");
        Employer employer = new Employer();
        employer.setName(request.getName());
        employer.setAddress(request.getAddress());
        employerService.create(request);


    }

    @Test
    public void testShow() {
        Long employerId = 1L;
        Employer employer = new Employer("Employer 1", "Address 1");
        when(employerRepo.findById(employerId)).thenReturn(Optional.of(employer));

        List<EmployerResponse> responses = employerService.show(employerId);

        assertEquals(1, responses.size());
        assertEquals("Employer 1", responses.get(0).getName());

        verify(employerRepo, times(1)).findById(employerId);
    }

    @Test
    public void testDeleteAll() {
        employerService.deleteAll();

        verify(employerRepo, times(1)).deleteAll();
    }

    @Test
    public void testEmployerCreate() {
        Employer employer = new Employer("Employer 1", "Address 1");
        employerService.employerCreate(employer);

        verify(employerRepo, times(1)).save(employer);
    }
}