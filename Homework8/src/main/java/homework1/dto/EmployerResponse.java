package homework1.dto;

import lombok.Data;

import java.util.Date;

@Data
public class EmployerResponse {
  private Long id;
  private String name;
  private String address;


  public EmployerResponse(String name, String address) {
    this.name = name;
    this.address = address;
  }
  public EmployerResponse() {
  }


}
