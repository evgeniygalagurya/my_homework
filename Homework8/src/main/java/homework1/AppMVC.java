package homework1;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppMVC implements WebMvcConfigurer {

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    String[][] mappings = {
        //   HTTP      template file
        {"/",      "navigation" },
        {"/navigation", "navigation"},
//        {"/home",  "home" },
//        {"/me",    "me"   },
//        {"/news",  "news" },
//        {"/admin", "admin"},
    };

    for (String[] item: mappings) {
      registry.addViewController(item[0]).setViewName(item[1]);
    }

  }
}
