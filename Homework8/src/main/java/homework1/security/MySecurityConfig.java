package homework1.security;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Log4j2
@Configuration
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

  private final PasswordEncoder enc;
  private final DbUserRepo repo;

  private void createOnce(){
    repo.saveAll(List.of(
            new DbUser("Evgeniy", enc.encode("123"), "ADMIN"),
            new DbUser("John", enc.encode("234"), "USER"),
            new DbUser("Jack", enc.encode("345"), "ADMIN", "USER"),
            new DbUser("Jeremy", enc.encode("456"))
        )
    );
  }

  public MySecurityConfig(DbUserRepo repo, PasswordEncoder enc) {
    this.repo = repo;
    this.enc = enc;
//    createOnce();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests()
//        .antMatchers("/resources/**").permitAll()
//        .antMatchers("/guest/**").permitAll()
//        .antMatchers("/home/**").authenticated()
        .antMatchers("/navigation/**").hasRole("ADMIN")
//        .antMatchers("/me/**").hasRole("USER")
//        .antMatchers("/news/**").hasAnyRole("ADMIN", "USER")
        .anyRequest().authenticated();

    http
        .formLogin().permitAll();
  }

}
