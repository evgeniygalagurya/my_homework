package homework1.controller;

import homework1.dto.EmployerRequest;
import homework1.dto.EmployerResponse;
import homework1.models.Employer;
import homework1.models.InputItems;
import homework1.service.EmployerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.spi.ErrorMessage;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Log4j2
@Controller
@RequiredArgsConstructor
@ControllerAdvice
public class EmployerController {

    org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(EmployerController.class);
    private final EmployerService employerService;
    private final EntityManager em;
    @GetMapping("employer")
    @ResponseBody
    public List<EmployerResponse> handleGet() {
        logger.info("Show all employer");
        return employerService.findAll();
    }

    @GetMapping("employer/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        employerService.delete(id);
        logger.info("Employer with id {} was deleted", id);
        return "Employer deleted";
    }

    @PostMapping("employer/create")
    @ResponseBody
    public void create(@RequestBody EmployerRequest p) {
        logger.info("Employer was created with name {}", p.getName());
        employerService.create(p);
    }

    @GetMapping("employer/show/{id}")
    @ResponseBody
    public List<EmployerResponse> show(@PathVariable("id") Long id) {
        logger.info("Show employer with id {}", id);
        return employerService.show(id);
    }

    @GetMapping("employer/delete-all")
    @ResponseBody
    public String deleteAll() {
        employerService.deleteAll();
        logger.info("All employer deleted");
        return "All employer deleted";
    }

    @GetMapping("employer-create")
    public String employerCreate(EmployerRequest employerRequest) {
        return "employer-create";
    }

    @PostMapping("employer-create")
    public String formValidate(InputItems form, HttpServletRequest rq, @Valid EmployerRequest employerRequest, BindingResult result) {
//        System.out.println(personForm);
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            fieldErrors.forEach(System.out::println);
            return "employer-create";
        }
        Employer employer = new Employer(form.getName(), form.getAddress());
        employerService.employerCreate(employer);
        logger.info("Employer was created with name {}", employer.getName());
        return "redirect:navigation";
    }


}

