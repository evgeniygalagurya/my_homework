package homework1.controller;

import homework1.dto.AccountRequest;
import homework1.dto.AccountResponseDto;
import homework1.models.Account;
import homework1.models.InputItems;
import homework1.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Log4j2
@Controller
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;
    private final SimpMessagingTemplate messagingTemplate;

    org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AccountController.class);

    @GetMapping("account")
    @ResponseBody
    public List<AccountResponseDto> handleGet() {
        logger.info("Show all account");
        return accountService.findAll();
    }
    @GetMapping("account/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        accountService.delete(id);
        logger.info("Account with id {} was deleted", id);
        return "Account deleted";
    }
    @PostMapping("account/create")
    @ResponseBody
    public void create(@RequestBody AccountRequest p) {
        logger.info("Account was created with number {}", p.getNumber());
        accountService.create(p);
    }
    @GetMapping("account/show/{id}")
    @ResponseBody
    public List<AccountResponseDto> show(@PathVariable("id") Long id) {
        logger.info("Show account with id {}", id);
        return accountService.show(id);
    }
    @GetMapping("account/delete-all")
    @ResponseBody
    public String deleteAll() {
        accountService.deleteAll();
        logger.info("All account deleted");
        return "All account deleted";
    }
    @GetMapping("account-create")
    public String accountCreate() {
        return "account-create";
    }
    @PostMapping("account-create")

    public String accountCreate(InputItems form, HttpServletRequest rq) {
        Account a = new Account(form.getInputCurrency());
        accountService.accountCreate((long) form.getIdCustomer(), a);
        logger.info("Account was created with number {}", a.getNumber());
        return "redirect:navigation";
    }

    @GetMapping("refill")
    public String moneyPut() {
        return "refill";
    }
    @PostMapping("refill")
//    @ResponseBody
    public String moneyPut(InputItems form, HttpServletRequest rq) throws InterruptedException {
        accountService.refillAccount(accountService.getAccountById(form.getIdAccount()), form.getValueM());
        logger.info("Account with id {} was refill on {}", form.getIdAccount(), form.getValueM());

        messagingTemplate.convertAndSend("/topic/account-status", "Account balance updated");

        return "redirect:navigation";
    }

    @GetMapping("withdraw")
    public String moneyGet() {
        return "withdraw";
    }
    @PostMapping("withdraw")
//    @ResponseBody
    public String moneyGet(InputItems form, HttpServletRequest rq) {
        accountService.withdrawAccount(accountService.getAccountById(form.getIdAccount()), form.getValueM());
        logger.info("Account with id {} was withdraw on {}", form.getIdAccount(), form.getValueM());
        return "redirect:navigation";
    }

    @GetMapping("transfer")
    public String moneyTransfer() {
        return "transfer";
    }
    @PostMapping("transfer")
//    @ResponseBody
    public String moneyTransfer(InputItems form, HttpServletRequest rq) {
        accountService.transfer(accountService.getAccountById(form.getIdAccount()), form.getValueM(), accountService.getAccountById(form.getIdAccount2()));
        logger.info("Transfer {} from account Id {} to account Id {}", form.getValueM(), form.getIdAccount(), form.getIdAccount2());
        return "redirect:navigation";
    }

}

