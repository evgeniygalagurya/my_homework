package homework1.controller;

import homework1.dto.CustomerMapper;
import homework1.dto.CustomerRequest;
import homework1.dto.CustomerResponseDto;
import homework1.models.Customer;
import homework1.models.InputItems;
import homework1.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Controller
@RequiredArgsConstructor
@ControllerAdvice
public class CustomerController {

    org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CustomerController.class);

    private final CustomerService customerService;
    private final CustomerMapper dtoCustomerMapper;
    private final PasswordEncoder enc;
    @GetMapping("customer")
    @ResponseBody
    public List<CustomerResponseDto> showAll() {
        logger.info("Show all customer");
        return customerService.showAll();
    }

    @GetMapping("customer/{page}/{size}")
    @ResponseBody
    public ResponseEntity<?> showAll(@PathVariable("page") Integer page, @PathVariable("size") Integer size) {
        List<Customer> customers = customerService.showAll1(page, size);
        List<CustomerResponseDto> departmentsDto = customers.stream().map(dtoCustomerMapper::convertToDto).collect(Collectors.toList());
        logger.info("Show employer on page {} with size {}", page, size);
        return ResponseEntity.ok(departmentsDto);
    }

    @GetMapping("customer/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        customerService.delete(id);
        logger.info("Customer with id {} was deleted", id);
        return "Customer deleted";
    }

    @PostMapping("customer/create")
    @ResponseBody
    public void create(@RequestBody CustomerRequest c) {
        logger.info("Customer was created with name {}", c.getName());
        customerService.create(c);
    }

    @GetMapping("customer/show/{id}")
    @ResponseBody
    public List<CustomerResponseDto> showId(@PathVariable("id") Long id) {
        logger.info("Show customer with id {}", id);
        return customerService.show(id);
    }

    @GetMapping("customer/delete-all")
    @ResponseBody
    public String deleteAll() {
        customerService.deleteAll();
        logger.info("All customer deleted");
        return "All customer deleted";
    }

    @GetMapping("customer-create")
    public String customCreate(CustomerRequest customerRequest) {
        return "customer-create";
    }

    @PostMapping("customer-create")
    public String customCreate(InputItems form, HttpServletRequest rq, @Valid CustomerRequest customerRequest, BindingResult result) {
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            fieldErrors.forEach(System.out::println);
            return "customer-create";
        }
        Customer customer = new Customer(form.getName(), form.getAge(), form.getEmail(), form.getPhoneNumber(), enc.encode(form.getPassword()));
        customerService.create1(customer, (long) form.getIdEmployer());
        logger.info("Customer was created with name {}", customer.getName());
        return "redirect:navigation";
    }
}

