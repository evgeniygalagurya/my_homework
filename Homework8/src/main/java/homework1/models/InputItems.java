package homework1.models;

import lombok.Data;

@Data
public class InputItems {

  String password;
  String name;
  String email;
  Integer age;
  String phoneNumber;
  Double valueM;
  String address;
  Currency inputCurrency;
  int idCustomer;
  int idAccount;
  int idAccount2;
  int idEmployer;
  int checkPage;


}
