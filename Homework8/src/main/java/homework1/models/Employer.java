package homework1.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name="Employer")
public class Employer extends AbstractEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;

    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
            name = "employee_customer",
            joinColumns = { @JoinColumn(name = "employee_id") },
            inverseJoinColumns = { @JoinColumn(name = "customer_id") })

    private Set<Customer> customers = new HashSet<>();


    public void addCustomer(Customer customer) {
        this.customers.add(customer);
        customer.getEmployers().add(this);
    }
    public void removeCustomer(Customer customer){
        this.customers.remove(customer);
        customer.getEmployers().remove(this);
    }

    public Employer() {
    }
    public Employer(String name, String address) {
        this.name = name;
        this.address = address;
    }

}
