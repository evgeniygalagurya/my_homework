package homework1.models;

public enum  Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
