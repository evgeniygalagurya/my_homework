package homework1.service;

import homework1.dto.EmployerRequest;
import homework1.dto.EmployerResponse;
import homework1.models.Employer;
import homework1.repo.EmployerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployerService {

  private final EmployerRepo employerRepo;

  public List<EmployerResponse> findAll() {
    return employerRepo.findAll()
        .stream()
        .map(p -> {
            EmployerResponse pp = new EmployerResponse() {{
            setId(p.getId());
            setName(p.getName());
            setAddress(p.getAddress());
         }};
          return pp;
        })
        .filter(p -> p.getName() != null)
        .filter(p -> !p.getName().isBlank())
        .toList();
  }
    public void delete(Long id) {
        employerRepo.deleteById(id);
    }
    public void create(EmployerRequest e) {
        Employer employer = new Employer();
        employer.setName(e.getName());
        employer.setAddress(e.getAddress());
        employerRepo.save(employer);
    }
    public List<EmployerResponse> show(Long id) {
        return employerRepo.findById(id)
                .stream()
                .map(p -> {
                    EmployerResponse pp = new EmployerResponse() {{
                        setId(p.getId());
                        setName(p.getName());
                        setAddress(p.getAddress());
                    }};
                    return pp;
                })
                .filter(p -> p.getName() != null)
                .filter(p -> !p.getName().isBlank())
                .toList();
    }

    public void deleteAll() {
        employerRepo.deleteAll();
    }

    public void employerCreate(Employer e) {
        employerRepo.save(e);
    }



}
