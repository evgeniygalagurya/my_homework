package homework1.service;

import homework1.dto.AccountRequest;
import homework1.dto.AccountResponseDto;
import homework1.models.Account;
import homework1.repo.AccountRepo;
import homework1.repo.CustomerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {

  private final AccountRepo accountRepo;
  private final CustomerRepo customerRepo;
  public List<AccountResponseDto> findAll() {
    return accountRepo.findAll()
        .stream()
        .map(p -> {
            AccountResponseDto as = new AccountResponseDto() {{
            setId(p.getId());
            setNumber(p.getNumber());
            setCurrency(p.getCurrency());
            setBalance(p.getBalance());
          }};
          return as;
        })
        .filter(p -> p.getNumber() != null)
        .filter(p -> !p.getNumber().isBlank())
        .toList();
  }
    public void delete(Long id) {
        accountRepo.deleteById(id);
    }
    public void create(AccountRequest c) {
        Account account = new Account();
        account.setNumber(c.getNumber());
        account.setCurrency(c.getCurrency());
        account.setBalance(c.getBalance());
        accountRepo.save(account);
    }
    public List<AccountResponseDto> show(Long id) {
        return accountRepo.findById(id)
                .stream()
                .map(p -> {
                    AccountResponseDto as = new AccountResponseDto() {{
                        setId(p.getId());
                        setNumber(p.getNumber());
                        setCurrency(p.getCurrency());
                        setBalance(p.getBalance());
                    }};
                    return as;
                })
                .filter(p -> p.getNumber() != null)
                .filter(p -> !p.getNumber().isBlank())
                .toList();
    }

    public void deleteAll() {
      accountRepo.deleteAll();
    }

    public void accountCreate(Long id, Account account) {
        customerRepo.getReferenceById(id).addAccounts(account);
        accountRepo.save(account);

}
    public Account getAccountById(int id) {
       return accountRepo.getReferenceById((long) id);
  }

  public void refillAccount(Account account, double valueM) {
        account.setBalance(account.getBalance() + valueM);
        accountRepo.save(account);
    }

    public void withdrawAccount(Account account, double valueM) {
        account.setBalance(account.getBalance() - valueM);
        accountRepo.save(account);
    }

    public void transfer(Account account, double valueM, Account account2) {
        account.setBalance(account.getBalance() - valueM);
        accountRepo.save(account);
        account2.setBalance(account2.getBalance() + valueM);
        accountRepo.save(account2);
    }
}
