package homework1.controller;

import homework1.dto.EmployerRequest;
import homework1.dto.EmployerResponse;
import homework1.models.Employer;
import homework1.service.EmployerService;
import homework1.models.InputItems;
import homework1.controller.EmployerController;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@Log4j2
public class EmployerControllerTest {

    @Mock
    private EmployerService employerService;

    @InjectMocks
    private EmployerController employerController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testHandleGet() {
        List<EmployerResponse> responseList = new ArrayList<>();
        responseList.add(new EmployerResponse("Company A", "Address A"));
        responseList.add(new EmployerResponse("Company B", "Address B"));

        when(employerService.findAll()).thenReturn(responseList);

        List<EmployerResponse> result = employerController.handleGet();

        assertEquals(2, result.size());
        assertEquals("Company A", result.get(0).getName());
        assertEquals("Company B", result.get(1).getName());

        verify(employerService, times(1)).findAll();
    }

    @Test
    public void testDelete() {
        Long employerId = 1L;

        MockHttpServletRequest request = new MockHttpServletRequest();
        String result = employerController.delete(employerId);

        assertEquals("Employer deleted", result);
        verify(employerService, times(1)).delete(employerId);
    }

    @Test
    public void testCreate() {
        EmployerRequest request = new EmployerRequest("Company C", "Address C");
        employerController.create(request);

        verify(employerService, times(1)).create(request);
    }

    @Test
    public void testShow() {
        Long employerId = 1L;
        List<EmployerResponse> responseList = new ArrayList<>();
        responseList.add(new EmployerResponse("Company A", "Address A"));

        when(employerService.show(employerId)).thenReturn(responseList);

        List<EmployerResponse> result = employerController.show(employerId);

        assertEquals(1, result.size());
        assertEquals("Company A", result.get(0).getName());

        verify(employerService, times(1)).show(employerId);
    }

    @Test
    public void testDeleteAll() {
        String result = employerController.deleteAll();

        assertEquals("All employer deleted", result);
        verify(employerService, times(1)).deleteAll();
    }

    @Test
    public void testFormValidateWithValidInput() {
        InputItems form = new InputItems();
        form.setName("Company D");
        form.setAddress("Address D");

        BindingResult bindingResult = mock(BindingResult.class);

        String result = employerController.formValidate(form, new MockHttpServletRequest(), new EmployerRequest(), bindingResult);

        assertEquals("redirect:navigation", result);
        verify(bindingResult, times(1)).hasErrors();
        verify(employerService, times(1)).employerCreate(any(Employer.class));
    }

    @Test
    public void testFormValidateWithInvalidInput() {
        InputItems form = new InputItems();
        form.setName("");
        form.setAddress("");

        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(true);

        String result = employerController.formValidate(form, new MockHttpServletRequest(), new EmployerRequest(), bindingResult);

        assertEquals("employer-create", result);
        verify(bindingResult, times(1)).hasErrors();
        verify(employerService, times(0)).employerCreate(any(Employer.class));
    }
}