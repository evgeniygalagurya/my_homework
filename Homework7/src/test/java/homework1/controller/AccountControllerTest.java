package homework1.controller;

import homework1.dto.AccountRequest;
import homework1.dto.AccountResponseDto;
import homework1.models.Account;
import homework1.models.Currency;
import homework1.models.InputItems;
import homework1.service.AccountService;
import homework1.controller.AccountController;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static homework1.models.Currency.UAH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@Log4j2
public class AccountControllerTest {

    @Mock
    private AccountService accountService;

    @InjectMocks
    private AccountController accountController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testHandleGet() {
        List<AccountResponseDto> responseList = new ArrayList<>();
        responseList.add(new AccountResponseDto(1L, "12345", Currency.EUR, 188.0));
        responseList.add(new AccountResponseDto(2L, "67890", Currency.GBP, 136.0));

        when(accountService.findAll()).thenReturn(responseList);

        List<AccountResponseDto> result = accountController.handleGet();
//        System.out.println(responseList);
        assertEquals(2, responseList.size());
        assertEquals(188.0, responseList.get(0).getBalance());
        assertEquals(136.0, responseList.get(1).getBalance());

        verify(accountService, times(1)).findAll();
    }

    @Test
    public void testDelete() {
        Long accountId = 1L;
        when(accountService.getAccountById(Math.toIntExact(accountId))).thenReturn(new Account(Currency.EUR));

        String response = accountController.delete(accountId);

        assertEquals("Account deleted", response);
        verify(accountService, times(1)).delete(accountId);
    }

    @Test
    public void testCreate() {
        AccountRequest request = new AccountRequest();

        MockHttpServletRequest httpRequest = new MockHttpServletRequest();
        when(accountService.getAccountById((int) anyLong())).thenReturn(new Account());
        accountController.create(request);

        verify(accountService, times(1)).create(request);
    }

    @Test
    public void testShow() {
        Long accountId = 1L;
        List<AccountResponseDto> responseList = new ArrayList<>();
        responseList.add(new AccountResponseDto(accountId, "12345", UAH, 100.0));

        when(accountService.show(accountId)).thenReturn(responseList);

        List<AccountResponseDto> result = accountController.show(accountId);

        assertEquals(1, result.size());
        assertEquals(UAH, result.get(0).getCurrency());

        verify(accountService, times(1)).show(accountId);
    }

    @Test
    public void testDeleteAll() {
        String response = accountController.deleteAll();

        assertEquals("All account deleted", response);
        verify(accountService, times(1)).deleteAll();
    }

    @Test
    public void testAccountCreate() {
        InputItems form = new InputItems();
        form.setIdCustomer(1);
        form.setInputCurrency(Currency.valueOf("USD"));

        when(accountService.getAccountById((int) anyLong())).thenReturn(new Account());

        String result = accountController.accountCreate(form, new MockHttpServletRequest());

        assertEquals("redirect:navigation", result);
        verify(accountService, times(1)).accountCreate(anyLong(), any(Account.class));
    }

    @Test
    public void testMoneyPut() {
        InputItems form = new InputItems();
        form.setIdAccount(1);
        form.setValueM(50.0);

        Account account = new Account(Currency.EUR);
        when(accountService.getAccountById((int) anyLong())).thenReturn(account);

        String result = accountController.moneyPut(form, new MockHttpServletRequest());

        assertEquals("redirect:navigation", result);
//        verify(accountService, times(1)).refillAccount(account, 50.0);
    }

    @Test
    public void testMoneyGet() {
        InputItems form = new InputItems();
        form.setIdAccount(1);
        form.setValueM(30.0);

        Account account = new Account(Currency.EUR);
        when(accountService.getAccountById((int) anyLong())).thenReturn(account);

        String result = accountController.moneyGet(form, new MockHttpServletRequest());

        assertEquals("redirect:navigation", result);
//        verify(accountService, times(1)).withdrawAccount(account, 30.0);
    }

    @Test
    public void testMoneyTransfer() {
        InputItems form = new InputItems();
        form.setIdAccount(1);
        form.setIdAccount2(2);
        form.setValueM(50.0);

        Account sourceAccount = new Account(Currency.EUR);
        Account targetAccount = new Account(Currency.EUR);

        when(accountService.getAccountById(1)).thenReturn(sourceAccount);
        when(accountService.getAccountById(2)).thenReturn(targetAccount);

        String result = accountController.moneyTransfer(form, new MockHttpServletRequest());

        assertEquals("redirect:navigation", result);
        verify(accountService, times(1)).transfer(sourceAccount, 50.0, targetAccount);
    }



}