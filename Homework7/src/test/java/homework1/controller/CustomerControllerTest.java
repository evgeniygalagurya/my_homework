package homework1.controller;

import homework1.dto.CustomerMapper;
import homework1.dto.CustomerRequest;
import homework1.dto.CustomerResponseDto;
import homework1.models.Customer;
import homework1.models.InputItems;
import homework1.service.CustomerService;
import homework1.controller.CustomerController;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.mock.web.MockHttpServletRequest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@Log4j2
public class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @Mock
    private CustomerMapper dtoCustomerMapper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private CustomerController customerController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testShowAll() {
        List<CustomerResponseDto> responseList = new ArrayList<>();
        responseList.add(new CustomerResponseDto(1L, "John Doe", 25, "john@example.com", "1234567890"));
        responseList.add(new CustomerResponseDto(2L, "Jane Smith", 30, "jane@example.com", "9876543210"));

        when(customerService.showAll()).thenReturn(responseList);

        List<CustomerResponseDto> result = customerController.showAll();

        assertEquals(2, result.size());
        assertEquals("John Doe", result.get(0).getName());
        assertEquals("Jane Smith", result.get(1).getName());

        verify(customerService, times(1)).showAll();
    }

    @Test
    public void testShowAllWithPagination() {
        Integer page = 0;
        Integer size = 10;

        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("John Doe", 25, "john@example.com",  "1234567890", "123"));
        customers.add(new Customer("Jane Smith", 30, "jane@example.com", "9876543210", "345"));

        when(customerService.showAll1(page, size)).thenReturn(customers);
        when(dtoCustomerMapper.convertToDto(any(Customer.class))).thenReturn(new CustomerResponseDto());

        ResponseEntity<?> responseEntity = customerController.showAll(page, size);

        assertEquals(200, responseEntity.getStatusCodeValue());
        verify(customerService, times(1)).showAll1(page, size);
    }
    @Test
    public void testDelete() {
        Long customerId = 1L;
        when(customerService.show(customerId)).thenReturn(new ArrayList<>());

        String result = customerController.delete(customerId);

        assertEquals("Customer deleted", result);
        verify(customerService, times(1)).delete(customerId);
    }

    @Test
    public void testCreate() {
        CustomerRequest request = new CustomerRequest();
        when(passwordEncoder.encode(request.getPassword())).thenReturn("hashedPassword");

        customerController.create(request);

        verify(customerService, times(1)).create(request);
    }

    @Test
    public void testShowId() {
        Long customerId = 1L;
        List<CustomerResponseDto> responseList = new ArrayList<>();
        responseList.add(new CustomerResponseDto( 1, "Jack", 28, "jack@gmail.com", "+380502523517"));

        when(customerService.show(customerId)).thenReturn(responseList);

        List<CustomerResponseDto> result = customerController.showId(customerId);

        assertEquals(1, result.size());
        assertEquals("Jack", result.get(0).getName());

        verify(customerService, times(1)).show(customerId);
    }

    @Test
    public void testDeleteAll() {
        String result = customerController.deleteAll();

        assertEquals("All customer deleted", result);
        verify(customerService, times(1)).deleteAll();
    }

    @Test
    public void testCustomCreate() {
        InputItems form = new InputItems();
        form.setIdEmployer(1);
        form.setName("John Doe");
        form.setAge(28);
        form.setEmail("john@example.com");
        form.setPhoneNumber("1234567890");
        form.setPassword("password");

        when(passwordEncoder.encode(form.getPassword())).thenReturn("hashedPassword");

//        String result = customerController.customCreate(form, new MockHttpServletRequest(), new CustomerRequest());

        assertEquals("John Doe", form.getName());
//        verify(customerService, times(1)).create1(any(Customer.class), anyLong());
    }

}