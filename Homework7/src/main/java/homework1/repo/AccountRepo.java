package homework1.repo;

import homework1.models.Account;
import homework1.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepo extends JpaRepository <Account, Long> {

}
