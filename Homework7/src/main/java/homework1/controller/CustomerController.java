package homework1.controller;

import homework1.dto.CustomerMapper;
import homework1.dto.CustomerRequest;
import homework1.dto.CustomerResponseDto;
import homework1.models.Customer;
import homework1.models.InputItems;
import homework1.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Controller
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerMapper dtoCustomerMapper;
    private final PasswordEncoder enc;
    @GetMapping("customer")
    @ResponseBody
    public List<CustomerResponseDto> showAll() {
        return customerService.showAll();
    }

    @GetMapping("customer/{page}/{size}")
    @ResponseBody
    public ResponseEntity<?> showAll(@PathVariable("page") Integer page, @PathVariable("size") Integer size) {
        List<Customer> customers = customerService.showAll1(page, size);
        List<CustomerResponseDto> departmentsDto = customers.stream().map(dtoCustomerMapper::convertToDto).collect(Collectors.toList());
        return ResponseEntity.ok(departmentsDto);
    }

    @GetMapping("customer/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        customerService.delete(id);
        return "Customer deleted";
    }

    @PostMapping("customer/create")
    @ResponseBody
    public void create(@RequestBody CustomerRequest c) {
        customerService.create(c);
    }

    @GetMapping("customer/show/{id}")
    @ResponseBody
    public List<CustomerResponseDto> showId(@PathVariable("id") Long id) {
        return customerService.show(id);
    }

    @GetMapping("customer/delete-all")
    @ResponseBody
    public String deleteAll() {
        customerService.deleteAll();
        return "All customer deleted";
    }

    @GetMapping("customer-create")
    public String customCreate(CustomerRequest customerRequest) {
        return "customer-create";
    }

    @PostMapping("customer-create")
    public String customCreate(InputItems form, HttpServletRequest rq, @Valid CustomerRequest customerRequest, BindingResult result) {
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            fieldErrors.forEach(System.out::println);
            return "customer-create";
        }
        Customer customer = new Customer(form.getName(), form.getAge(), form.getEmail(), form.getPhoneNumber(), enc.encode(form.getPassword()));
        customerService.create1(customer, (long) form.getIdEmployer());
        return "redirect:navigation";
    }
}

