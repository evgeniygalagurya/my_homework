package homework4;

import java.util.Arrays;


public class Human {

    private String name;
    private String surname;
    private Integer year;
    private Integer iq;
    private String[][] schedule;
    private Family family;

    public Human (String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;

    }
    public Human (Family family, String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
    }
    public Human (String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;

    }
    public Human (Family family, String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }
    public Human () {
    }
    public String getName() {return name;}
    public String getSurname() {return surname;}
    public Integer getYear() {return year;}
    public Integer getIq() {
        return iq;
    }
    public void setName(String newName) {name = newName;}
    public void setSurname(String newSurname) {
        surname = newSurname;
    }
    public void setYear(Integer newYear) {
        year = newYear;
    }
    public void setIq(Integer newIq) {
        iq = newIq;
    }

    @Override
    public String toString() {
           return "Human{name = " + name + ", surname = " + surname + ", year = " + year + ", iq = " + iq +
                ", shedule=" + Arrays.deepToString((schedule)) + "}";
    }
    @Override
    public int hashCode() {
        int result = name == null ? 0 : name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + year;
        result = 31 * result + iq;
        result = 31 * result + Arrays.deepHashCode(schedule);
        return result;
    }

    @Override
    public boolean equals(Object that0) {
        if (that0 == null) return false;
        if (this == that0) return true;
        if (!(that0.getClass().equals(this.getClass()))) return false;
        Human that = (Human) that0;
        return this.name.equals(that.name) &&
                this.surname.equals(that.surname) &&
                this.year.equals(that.year) &&
                this.iq.equals(that.iq) &&
                Arrays.deepEquals(this.schedule, that.schedule);
    }

}