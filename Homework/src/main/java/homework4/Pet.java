package homework4;
import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private Integer age;
    private Integer trickLevel;
    private String[] habits;

    public Pet (String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet (String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    public Pet () {

    }
    public String getSpecies() {
        return species;
    }
    public String getNickname() {
        return nickname;
    }
    public Integer getAge() {
        return age;
    }
    public Integer getTrickLevel() {
        return trickLevel;
    }
    public void setSpecies(String newSpecies) {
        species = newSpecies;
    }
    public void setNickname(String newNickname) {
        nickname = newNickname;
    }
    public void setAge(Integer newAge) {
        age = newAge;
    }
    public void setTrickLevel(Integer newTrickLevel) {
        trickLevel = newTrickLevel;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + nickname + " Я соскучился.");
    }
    public void foul() {
        System.out.println("Нужно хорошо замести следы.....");
    }
    @Override
    public String toString() {
        if (habits.length == 0)
            return species + "{nickname = " + nickname + "}";
        return species + "{nickname = " + nickname + ", age = " + age + ", trickLevel = " + trickLevel + ", habits = " + Arrays.toString(habits) + "}";
    }
    @Override
    public int hashCode() {
        int result = species == null ? 0 : species.hashCode();
        result = 31 * result + nickname.hashCode();
        result = 31 * result + age;
        result = 31 * result + trickLevel;
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }
    @Override
    public boolean equals(Object that0) {
        if (that0 == null) return false;
        if (this == that0) return true;
        if (!(that0.getClass().equals(this.getClass()))) return false;
        Pet that = (Pet) that0;
        return this.species.equals(that.species) &&
                this.nickname.equals(that.nickname) &&
                this.age.equals(that.age) &&
                this.trickLevel.equals(that.trickLevel) &&
                Arrays.equals(this.habits, that.habits);
    }
}
