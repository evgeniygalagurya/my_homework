package homework8;

public interface FamilyDao<A extends Identifable> {

    void getAllFamilies() throws Exception;

    void getFamilyByIndex(int id) throws Exception;

    void deleteFamily(int id) throws Exception;

    void deleteFamily(A f) throws Exception;

    void saveFamily(A f) throws Exception;

    void getFamiliesBiggerThan(int n) throws Exception;

    void getFamiliesLessThan(int n) throws Exception;

    void countFamiliesWithMemberNumber(int n) throws Exception;

    void createNewFamily(Human mother, Human father) throws Exception;

    void bornChild(int id, String nameBoy, String nameGirl) throws Exception;

    void adoptChild(int id, Human child) throws Exception;

    void deleteAllChildrenOlderThen(int years) throws Exception;

    void count() throws Exception ;

    void getPets(int id) throws Exception;

    void addPet(int id, Pet pet) throws Exception;

//    default void addChild(Human child) {
//        children.add(child);
//    }
//    default void deleteChild(int idx) {
//        children.remove(idx);
//    }
//    default void deleteChild(Human child) {
//        children.remove(child);
//    }
//    default void countFamily() {
//        int c = 2 + children.size();
//        System.out.println(c);
    }

