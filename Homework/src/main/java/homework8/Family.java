package homework8;

import java.util.ArrayList;
import java.util.HashSet;


public record Family(int id, Human mother, Human father, HashSet<Pet> pet, ArrayList<Human> children) implements IdentifableSerializable {


    public void addChild(Human child) {
        children.add(child);
    }
    public void deleteChild(int idx) {
        children.remove(idx);
    }
    public void deleteChild(Human child) {
        children.remove(child);
    }
    void countFamily() {
        int c = 2 + children.size();
        System.out.println(c);
    }

    @Override
    public String toString() {
        return "\n" +
                id + ". family: " + father.getSurname() + "\n" +
                "   mother: {name = " + mother.getName() + ", surname = " + mother.getSurname() + ", year = " + mother.getYear() + "}\n" +
                "   father: {name = " + father.getName() + ", surname = " + father.getSurname() + ", year = " + father.getYear() + "}\n" +
                "   children: " + children.toString() + "\n" +
                "   pets: " + pet;
    }

}
