package homework8;

import java.util.*;


public class lesson5Main    {

    public static void main(String[] args) throws Exception {
        FamilyDao<Family> familyList = new CollectionFamilyDao<>();
        Pet cat = new Pet(Species.CAT, "Мурчик", 5, 55, new HashSet<>(Collections.singleton("веселый и быстрый")));
        Pet dog = new Pet(Species.DOG, "Мухтар", 8, 70, new HashSet<>(Collections.singleton("храбрый и быстрый")));
        Pet mouse = new Pet(Species.MOUSE, "Мышка", 4, 45, new HashSet<>(Collections.singleton("шустрая")));
        Human sun1 = new Human("Глеб", "Яковлев", 1982, 85, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "отдых", DayOfWeek.WEDNESDAY, "вечеринка")));
        Human sun2 = new Human("Артем", "Яковлев", 1986, 87, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "баскетбол", DayOfWeek.WEDNESDAY, "вечеринка")));
        Human daughter = new Human("Анастасия", "Надежная", 2002, 84, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "танцы", DayOfWeek.WEDNESDAY, "вечеринка")));
        Family first = new Family(1, new Human("Mariya", "Petrova", 1953, 75), new Human("Ivan", "Petrov", 1950, 80), new HashSet<Pet>(Set.of(cat)), new ArrayList<Human>());
        Family second = new Family(2, new Human("Helen", "Good", 1986, 75), new Human("Egor", "Good", 1982, 80), new HashSet<Pet>(Set.of(dog)), new ArrayList<Human>());
        Family fierd = new Family(3,new Human("Olga", "Bitner", 1984, 75), new Human("Evgen", "Bitner", 1980, 90), new HashSet<Pet>(Set.of(mouse)), new ArrayList<Human>());
//
//        familyList.saveFamily(first);
//        familyList.saveFamily(second);
//        familyList.saveFamily(fierd);
//        first.countFamily();
        familyList.createNewFamily(sun1, daughter);
        familyList.createNewFamily(daughter, sun2);
//        familyList.adoptChild(4, sun1);
//        familyList.getAllFamilies();

//        familyList.deleteFamily(first);

//        familyList.getAllFamilies();
//        familyList.count();
        familyList.getFamilyByIndex(2);










//        System.out.println(cat);
//        System.out.println(sun1);
//        System.out.println(first);
//        first.addChild(sun1);
//        first.addChild(sun2);
//        first.addChild(daughter);
//        System.out.println(first);
//        first.deleteChild(0);
//        System.out.println(first);
//        first.countFamily();
    }
}
