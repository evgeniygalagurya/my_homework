package homework3;

import java.io.*;
import java.util.Scanner;

public class TaskPlanner {
    public static String representCell(String value) {
        if (value == "a") return "Sunday";
        if (value == "b") return "do home work";
        if (value == "c") return "Monday";
        if (value == "d") return "go to courses";
        if (value == "f") return "Tuesday";
        if (value == "g") return "watch a film";
        if (value == "h") return "Wednesday";
        if (value == "i") return "walk in the park";
        if (value == "j") return "Thursday";
        if (value == "k") return "go to the club";
        if (value == "o") return "Friday";
        if (value == "p") return "play with my dog";
        if (value == "q") return "Saturday";
        if (value == "r") return "go to my parents";
        return "-";
    }

    public static void printBoard(String[][] scedule) {
        System.out.println("------------------------------------");
        for (int y = 0; y < scedule.length; y++) {
            System.out.print("|");
            for (int x = 0; x < scedule[y].length; x++) {
                System.out.printf("%-16s|", representCell(scedule[y][x]));
            }
            System.out.println();
            System.out.println("-----------------------------------");
        }
    }

    public static void fillBoard(String[][] scedule) {
        scedule[0][0] = "a";
        scedule[0][1] = "b";
        scedule[1][0] = "c";
        scedule[1][1] = "d";
        scedule[2][0] = "f";
        scedule[2][1] = "g";
        scedule[3][0] = "h";
        scedule[3][1] = "i";
        scedule[4][0] = "j";
        scedule[4][1] = "k";
        scedule[5][0] = "o";
        scedule[5][1] = "p";
        scedule[6][0] = "q";
        scedule[6][1] = "r";
    }

    public static void main(String[] args) {
        String[][] scedule = new String[7][2];
        fillBoard(scedule);
        //printBoard(scedule);

        //String message = "";


        while (true) {
            System.out.println("Please, input the day of the week:");
            Scanner s = new Scanner(System.in);
            String day = s.nextLine();
            day = day.substring(0, 1).toUpperCase() + day.substring(1);
            day = day.trim();
            String message = switch (day) {
                case "Sunday" -> ("Your tasks for " + day + ": " + representCell(scedule[0][1]));
                case "Monday" -> ("Your tasks for " + day + ": " + representCell(scedule[1][1]));
                case "Tuesday" -> ("Your tasks for " + day + ": " + representCell(scedule[2][1]));
                case "Wednesday" -> ("Your tasks for " + day + ": " + representCell(scedule[3][1]));
                case "Thursday" -> ("Your tasks for " + day + ": " + representCell(scedule[4][1]));
                case "Friday" -> ("Your tasks for " + day + ": " + representCell(scedule[5][1]));
                case "Saturday" -> ("Your tasks for " + day + ": " + representCell(scedule[6][1]));
                case "Exit" -> "";
                default -> "Sorry, I don't understand you, please try again.";
            };
            System.out.println(message);
            if (message == "") break;
        }

    }

}

