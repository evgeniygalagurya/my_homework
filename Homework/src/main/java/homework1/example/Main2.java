package homework1.example;

import java.util.Random;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        System.out.println("Let the game begin!");
        Scanner in = new Scanner(System.in);
        System.out.print("Input your name please: ");
        String name = in.nextLine();


        Random rnd = new Random();
        int[] AI = new int[100];
        for (int i=0; i<AI.length; i++)
            AI[i] = Math.abs(rnd.nextInt()) % (100 + 1);


        while (true) {
            System.out.print("Input the number from " + 0 + " up to " + 100 + ": ");
            int num = in.nextInt();

            if (num > 100) {
                System.out.println("Your number is not in range. Please, try again. ");
            } else if (num == AI[1]) {
                System.out.println("Congratulations " + name + "!");
                break;
            } else if (num > AI[1]) {
                System.out.println("Your number is too big. Please, try again.");
            } else if (num < AI[1]) {
                System.out.println("Your number is too small. Please, try again.");
            } else {
            }
        }

         //System.out.println(AI[1]);
    }
}