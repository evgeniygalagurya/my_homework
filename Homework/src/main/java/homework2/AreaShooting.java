package homework2;
import java.util.Random;
import java.util.Scanner;
public class AreaShooting {


    public static String representCell(int value) {
        if (value == 6) return "0";
        if (value == 1) return "1";
        if (value == 2) return "2";
        if (value == 3) return "3";
        if (value == 4) return "4";
        if (value == 5) return "5";
        if (value == 7) return "*";
        if (value == 8) return "X";
        return "-";
    }

    public static void printBoard(int[][] board) {
        System.out.println("-------------------------");
        for (int y = 0; y < board.length; y++) {
            System.out.print("|");
            for (int x = 0; x < board[y].length; x++) {
                System.out.printf(" %s |", representCell(board[y][x]));
            }
            System.out.println();
            System.out.println("-------------------------");
        }
    }

    public static void fillBoard(int[][] board) {
        board[0][0] = 6;
        board[0][1] = 1;
        board[0][2] = 2;
        board[0][3] = 3;
        board[0][4] = 4;
        board[0][5] = 5;
        board[1][0] = 1;
        board[2][0] = 2;
        board[3][0] = 3;
        board[4][0] = 4;
        board[5][0] = 5;
    }

    public static void main(String[] args) {
        int[][] board = new int[6][6];
        fillBoard(board);
        //printBoard(board);
        Scanner in = new Scanner(System.in);
        Random rnd = new Random();
        int[] AI = new int[5];
        for (int i=0; i<AI.length; i++)
            AI[i] = Math.abs(rnd.nextInt()) % (5 + 1);
        int[] BI = new int[5];
        for (int i=0; i<BI.length; i++)
            BI[i] = Math.abs(rnd.nextInt()) % (5 + 1);

        int a = AI[1];
        int b = BI[1];
        //System.out.println(AI[1]);
        //System.out.println(BI[1]);
        board[a][b] = 'X';
        System.out.println("All set. Get ready to rumble!");
        while (true) {
            System.out.print("Input the number of line to the fight: ");
            int line = in.nextInt();
            if (line <1 | line > 5) {
                System.out.println("Line number not in Area. Please try again");
                continue;
            }
            System.out.print("Input the number of column: ");
            int column = in.nextInt();
            if (column <1 | column > 5) {
                System.out.println("Str number not in Area. Please try again");
                continue;
            }
            if (board[line][column] == 'X') {
                System.out.println("Your win");
                board[line][column] = 8;
                printBoard(board);
                break;
            } else {
                board[line][column] = 7;
                printBoard(board);
                System.out.println("Try again");
                //
            }
        }
    }
}