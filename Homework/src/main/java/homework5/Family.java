package homework5;
import org.apache.commons.lang3.ArrayUtils;
import java.util.Arrays;


public class Family {
    private Human mother;
    private Human father;
    private Pet pet;
    Human[] children;
    int k = 0;

    public Family (Human mother, Human father, Pet pet, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.children = children;
    }

    public Family () {
    }

    public void addChild(Human[] children) {
        if (k < 1) {
            Human[] children2 = new Human[1];
            System.arraycopy(children, 0, children2, k, 1);
            this.children = children2;
        }
        if (k == 1 || k > 1) {
            Human[] children3 = new Human[k+1];
            children3 = ArrayUtils.addAll(this.children, children);
            this.children = children3;
        }
        k = k + 1;

    }
    public void deleteChild(int idx) {
        if (idx <= children.length - 1) {
            this.children = ArrayUtils.remove(this.children, idx);
        }
        this.children = children;
    }

    public void deleteChild(Human[] children) {
        String c = Arrays.toString(children);
        Human[] children4 = this.children;
        for (int i = 0; i < this.children.length; i++) {
            if (("[" + String.valueOf(this.children[i]) + "]").equals(c)) {
                this.children = ArrayUtils.remove(this.children, i);
                break;
            }
        }
    }
    public void greatPet() {
        System.out.println("Привет " + pet.getNickname());
    }
    public void describePet() {
        if (pet.getTrickLevel() > 50) {
            System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " лет, он очень хитрый");
        }
        if (pet.getTrickLevel() < 50) {
            System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " лет, он не очень хитрый");
        }
    }
    public int countFamily() {
        int c = 2 + children.length;
        System.out.println(c);
        return c;
    }

    @Override
    public String toString() {
        return "Семья " + father.getSurname() + "\n" +
                "mother{name = " + mother.getName() + ", surname = " + mother.getSurname() + ", year = " + mother.getYear() + "}\n" +
                "father{name = " + father.getName() + ", surname = " + father.getSurname() + ", year = " + father.getYear() + "}\n" +
                "Child = " + Arrays.toString(children) + "\n" +
                pet.toString();
    }
    @Override
    public int hashCode() {
        int result = mother == null ? 0 : mother.hashCode();
        result = 31 * result + father.hashCode();
        result = 31 * result + pet.hashCode();
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public boolean equals(Object that0) {
        if (that0 == null) return false;
        if (this == that0) return true;
        if (!(that0.getClass().equals(this.getClass()))) return false;
        Family that = (Family) that0;
        return this.mother.equals(that.mother) &&
                this.father.equals(that.father) &&
                this.pet.equals(that.pet) &&
                Arrays.equals(this.children, that.children);
    }
    @Override
    protected void finalize() {
        System.out.println("Семья " + father.getSurname() + " будет удалена навсегда");
    }


}
