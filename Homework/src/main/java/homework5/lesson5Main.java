package homework5;
import java.util.Arrays;

import static java.time.DayOfWeek.*;

public class lesson5Main {


    public static void main(String[] args) {
//        Human[] human = new Human[10000];
//        for(int i = 0; i < human.length; i++) {
//            human[i] = new Human();
//        }
//        human = null;
//        Human mother22 = new Human();
//        mother22 = null;
//        System.gc();


        Pet cat = new Pet(Species.CAT, "Мурчик", 5, 55, new String[]{"веселый", "четкий"});
        Pet dog = new Pet(Species.DOG, "Малыш", 4, 55, new String[]{"быстрый"});
        Pet dog1 = new Pet(Species.DOG, "Малыш", 4, 55, new String[]{"быстрый"});
        Pet mouse = new Pet(Species.MOUSE, "Норушка");
        Family nowname = new Family();
        Human father = new Human(nowname, "Иван", "Яковлев", 1973);
        Human[] sun1 = new Human[]{new Human("Глеб", "Яковлев", 1982, 85, new String[][]{{DayOfWeek.MONDAY.name(), "Шахматы"}})};
        Human[] sun2 = new Human[]{new Human("Артем", "Яковлев", 1986, 87, new String[][]{{DayOfWeek.THURSDAY.name(), "Спортзал"}})};
        Human[] daughter = new Human[]{new Human("Анастасия", "Надежная", 2002, 84, new String[][]{{DayOfWeek.WEDNESDAY.name(), "танцы"},{DayOfWeek.FRIDAY.name(), "дискотека"}})};
        Family first = new Family(new Human("Mariya", "Petrova", 1953, 75), new Human("Ivan", "Petrov", 1950, 80), cat, new Human[]{});
        Family first1 = new Family(new Human("Mariya", "Petrova", 1953, 75), new Human("Ivan", "Petrov", 1950, 80), cat, new Human[]{});
        Human sun4 = new Human("Глеб", "Яковлев", 1982, 85, new String[][]{{DayOfWeek.SUNDAY.name(), "свободный вечер"}, {DayOfWeek.FRIDAY.name(), "спортзал"}});
        Human sun5 = new Human("Глеб", "Яковлев", 1982, 85, new String[][]{{DayOfWeek.SUNDAY.name(), "свободный вечер"}, {DayOfWeek.FRIDAY.name(), "спортзал"}});
        Human mother2 = new Human("Елена", "Прекрасная", 1986, 91);
        Human father2 = new Human("Евгений", "Надежный", 1982, 90);
        Family second = new Family(mother2, father2, dog, new Human[]{});

//        System.out.println(Arrays.toString(sun1));
//        System.out.println(sun4);
//        first.addChild(sun1);
//        first.addChild(sun2);
//        System.out.println(Arrays.toString(first.children));
//        first.deleteChild(sun1);
//        System.out.println(Arrays.toString(first.children));
//        first.countFamily();
        System.out.println(cat);
       

    }
}
