package homework9;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.text.SimpleDateFormat;

public class Human {

    private String name;
    private String surname;
    private long birthDate;
    private Integer iq;

    public Human(String name, String surname, long birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    public void describeAge() {
        Period p = new Period(birthDate, System.currentTimeMillis(), PeriodType.yearMonthDay());
        p.normalizedStandard(PeriodType.yearMonthDay());
        int years = p.getYears();
        int month = p.getMonths();
        int day = p.getDays();
        System.out.println(years + " лет, " + month + " месяцев, " + day + " дней");
    }

    @Override
    public String toString() {
        return "Human{name = " + name + ", surname = " + surname + ", birthDate = " +  new SimpleDateFormat("dd/MM/yyyy").format(birthDate) + ", iq = " + iq + "}";
    }

}