package homework6;

import java.util.Arrays;

public abstract class Pet {
    private Species species;
    private String nickname;
    private Integer age;
    private Integer trickLevel;
    private String[] habits;

    public Pet (String nickname, int age, int trickLevel, String[] habits) {
//        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Species getSpecies() { return species; }
    public String getNickname() {
        return nickname;
    }
    public Integer getAge() {
        return age;
    }
    public Integer getTrickLevel() {
        return trickLevel;
    }
    public String[] getHabits() {return habits; }
    public void setSpecies(Species species) { this.species = species; }
    public void setNickname(String newNickname) { nickname = newNickname; }
    public void setAge(Integer newAge) {
        age = newAge;
    }
    public void setTrickLevel(Integer newTrickLevel) {
        trickLevel = newTrickLevel;
    }
    public void setHabits(String[] newHabits) {habits = newHabits; }


    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + nickname + " Я соскучился.");
    }
}
