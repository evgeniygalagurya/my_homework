package homework6;

import java.util.Arrays;

public class Fish extends Pet{
    public Fish(Species species, String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void respond() {
        super.respond();
    }
    @Override
    public String toString() {
        return Species.Fish + "{nickname = " + getNickname() + ", age = " + getAge() + ", trickLevel = " + getTrickLevel() + ", habits = " + Arrays.toString(getHabits()) + "}";
    }
}
