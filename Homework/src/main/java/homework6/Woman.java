package homework6;

public final class Woman extends Human{
    public Woman(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        super(name, surname, year, iq, schedule, pet);
    }
    @Override
    public void greatPet() {
        System.out.println("Hello my sweet" + pet.getNickname());
    }
    public void makeup() {
        System.out.println("Мне нужно к косметологу");
    }
}
