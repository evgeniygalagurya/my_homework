package homework6;

public enum Species {
    Fish,
    DomesticCat,
    Dog,
    Robocat,
    Unknown
}
