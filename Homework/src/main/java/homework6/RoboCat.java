package homework6;

import java.util.Arrays;

public class RoboCat extends Pet{
    public RoboCat(Species species, String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }
    @Override
    public void eat() {
        super.eat();
    }
    @Override
    public void respond() {
        super.respond();
    }
    @Override
    public String toString() {
        return Species.Robocat + "{nickname = " + getNickname() + ", age = " + getAge() + ", trickLevel = " + getTrickLevel() + ", habits = " + Arrays.toString(getHabits()) + "}";
    }

}
