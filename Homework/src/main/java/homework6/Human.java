package homework6;

import java.util.Arrays;

public abstract class Human {
    private String name;
    private String surname;
    private Integer year;
    private Integer iq;
    private String[][] schedule;
    Pet pet;
//        private Family family;

    public Human(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
//        this.family = family;
    }
    public String getName() { return name; }
    public String getSurname() { return surname; }
    public Integer getYear() { return year; }
    public Integer getIq() {
        return iq;
    }
    public void setName(String newName) { name = newName; }
    public void setSurname(String newSurname) {
        surname = newSurname;
    }
    public void setYear(Integer newYear) {
        year = newYear;
    }
    public void setIq(Integer newIq) {
        iq = newIq;
    }

    public void greatPet() {
        System.out.println("Привет " + pet.getNickname());
    }

//    @Override
//    public String toString() {
//        return "Human{name = " + name + ", surname = " + surname + ", year = " + year + ", iq = " + iq +
//                ", schedule=" + Arrays.deepToString((schedule)) + "}";
//    }
}

