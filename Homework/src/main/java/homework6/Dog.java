package homework6;

import java.util.Arrays;

public class Dog extends Pet implements Foul {
    public Dog(Species species, String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы.....");
    }
    @Override
    public void eat() {
        super.eat();
    }
    @Override
    public void respond() {
        super.respond();
    }
    @Override
    public String toString() {
        return Species.Dog + "{nickname = " + getNickname() + ", age = " + getAge() + ", trickLevel = " + getTrickLevel() + ", habits = " + Arrays.toString(getHabits()) + "}";
    }
}
