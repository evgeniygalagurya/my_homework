package homework6;

public final class Man extends Human{
    public Man(String name, String surname, int year, int iq, String[][] schedule, Pet pet) {
        super(name, surname, year, iq, schedule, pet);
    }
    @Override
    public void greatPet() {
        System.out.println("Hay" + pet.getNickname());
    }
    public void repairCar() {
        System.out.println("Я уехал на СТО чинить машину");
    }
}
