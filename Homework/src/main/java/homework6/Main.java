package homework6;

public class Main {
    public static void main(String[] args) {
        Fish fish = new Fish(Species.Fish, "Guppi", 1, 45, new String[]{"faster"});
        Dog dog = new Dog(Species.Dog, "Муха", 4, 62, new String[]{"добрый"});
        dog.foul();
        fish.eat();
        RoboCat cat = new RoboCat(Species.Robocat,"Rubby", 1, 88, new String[]{"fanny"});
        System.out.println(cat);
    }


}
