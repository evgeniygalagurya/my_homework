package homework7;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Pet {
    private Species species;
    private String nickname;
    private Integer age;
    private Integer trickLevel;
    private HashSet<String> habits;

    public Pet(Species species, String nickname, int age, int trickLevel, HashSet<String> habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    public Pet() {
    }

    public Species getSpecies() { return species; }
    public String getNickname() {
        return nickname;
    }
    public Integer getAge() {
        return age;
    }
    public Integer getTrickLevel() {
        return trickLevel;
    }
    public HashSet getHabits() {return habits; }
    public void setSpecies(Species species) { this.species = species; }
    public void setNickname(String newNickname) {
        nickname = newNickname;
    }
    public void setAge(Integer newAge) {
        age = newAge;
    }
    public void setTrickLevel(Integer newTrickLevel) {
        trickLevel = newTrickLevel;
    }
    public void setHabits(HashSet newHabits) {habits = newHabits; }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + nickname + " Я соскучился.");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы.....");
    }

    @Override
    public String toString() {
        return species + "{nickname = " + nickname + ", age = " + age + ", trickLevel = " + trickLevel + ", habits = " + habits + "}";
    }

}
