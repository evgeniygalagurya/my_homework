package homework7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;


public class Family {
    private Human mother;
    private Human father;
    private HashSet<Pet> pet;
    private ArrayList<Human> children;
    int k = 0;

    public Family(Human mother, Human father, HashSet<Pet> pet, ArrayList<Human> children) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.children = children;
    }

    public void addChild(Human child) {
        children.add(child);
    }

    public void deleteChild(int idx) {
        children.remove(idx);
    }

    public void deleteChild(Human child) {
        children.remove(child);
    }

    public int countFamily() {
        int c = 2 + children.size();
        System.out.println(c);
        return c;
    }

    @Override
    public String toString() {
        return "Семья " + father.getSurname() + "\n" +
                "mother{name = " + mother.getName() + ", surname = " + mother.getSurname() + ", year = " + mother.getYear() + "}\n" +
                "father{name = " + father.getName() + ", surname = " + father.getSurname() + ", year = " + father.getYear() + "}\n" +
                "Child = " + children.toString() + "\n" +
                "Pet = " + pet;
    }
}
