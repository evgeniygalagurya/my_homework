package homework7;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Pet cat = new Pet(Species.CAT, "Мурчик", 5, 55, new HashSet<>(Collections.singleton("веселый и быстрый")));
        Pet dog = new Pet(Species.DOG, "Мухтар", 8, 70, new HashSet<>(Collections.singleton("храбрый и быстрый")));
        Human sun1 = new Human("Глеб", "Яковлев", 1982, 85, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "отдых", DayOfWeek.WEDNESDAY, "вечеринка")));
        Human sun2 = new Human("Артем", "Яковлев", 1986, 87, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "баскетбол", DayOfWeek.WEDNESDAY, "вечеринка")));
        Human daughter = new Human("Анастасия", "Надежная", 2002, 84, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "танцы", DayOfWeek.WEDNESDAY, "вечеринка")));
        Family first = new Family(new Human("Mariya", "Petrova", 1953, 75), new Human("Ivan", "Petrov", 1950, 80), new HashSet<Pet>(Set.of(cat)), new ArrayList<Human>());

//        System.out.println(cat);
//        System.out.println(sun1);
//        System.out.println(first);
//        first.addChild(sun1);
//        first.addChild(sun2);
        first.addChild(daughter);
//        System.out.println(first);
//        first.deleteChild(0);
        System.out.println(first);
//        first.countFamily();
    }
}
