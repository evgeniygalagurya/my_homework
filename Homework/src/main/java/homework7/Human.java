package homework7;

import java.util.HashMap;
import java.util.TreeMap;

public class Human {

    private String name;
    private String surname;
    private Integer year;
    private Integer iq;
    private TreeMap<DayOfWeek, String> schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;

    }
    public Human(Family family, String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
    }
    public Human(String name, String surname, int year, int iq, TreeMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;

    }
    public Human(Family family, String name, String surname, int year, int iq, TreeMap<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }
    public Human() {
    }
    public String getName() {return name;}
    public String getSurname() {return surname;}
    public Integer getYear() {return year;}
    public Integer getIq() {
        return iq;
    }
    public void setName(String newName) {name = newName;}
    public void setSurname(String newSurname) {
        surname = newSurname;
    }
    public void setYear(Integer newYear) {
        year = newYear;
    }
    public void setIq(Integer newIq) {
        iq = newIq;
    }

    @Override
    public String toString() {
        return "Human{name = " + name + ", surname = " + surname + ", year = " + year + ", iq = " + iq +
                ", schedule=" + schedule + "}";
    }

}