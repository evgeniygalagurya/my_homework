package homework5;

import org.junit.jupiter.api.Test;

import static java.time.DayOfWeek.MONDAY;
import static java.time.DayOfWeek.SUNDAY;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {
    Human mother = new Human("Мария", "Яковлева", 1953,80);
    Human father = new Human("Иван", "Яковлев", 1973, 85);
    Pet cat = new Pet(Species.CAT, "Мурчик", 5, 55, new String[]{"веселый", "четкий"});
    Human sun = new Human("Артем", "Яковлев", 1986, 87, new String[][]{{MONDAY.name(), "party"}, {SUNDAY.name(), "rest"}});
    @Test
    public void testToString() {
        assertEquals(sun.toString(),
                "Human{name = Артем, surname = Яковлев, year = 1986, iq = 87, schedule=[[MONDAY, party], [SUNDAY, rest]]}");
    }
}