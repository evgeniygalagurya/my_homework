package homework5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {
    Pet cat = new Pet(Species.CAT, "Мурчик", 5, 55, new String[]{"веселый", "четкий"});
    @Test
    void testToString() {
        assertEquals(cat.toString(), "CAT{nickname = Мурчик, age = 5, trickLevel = 55, habits = [веселый, четкий]}");
    }
}