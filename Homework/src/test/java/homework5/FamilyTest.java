package homework5;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.time.DayOfWeek.MONDAY;
import static java.time.DayOfWeek.SUNDAY;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTest {
    Human mother = new Human("Мария", "Яковлева", 1953,80);
    Human father = new Human("Иван", "Яковлев", 1973, 85);
    Pet cat = new Pet(Species.CAT, "Мурчик", 5, 55, new String[]{"веселый", "четкий"});
    Family first = new Family(mother, father, cat, new Human[]{});
    Human[] sun2 = new Human[]{new Human("Артем", "Яковлев", 1986, 87, new String[][]{{DayOfWeek.THURSDAY.name(), "Спортзал"}})};
    Human[] sun1 = new Human[]{new Human("Глеб", "Яковлев", 1982, 85, new String[][]{{DayOfWeek.MONDAY.name(), "Шахматы"}})};
    Human[] daughter = new Human[]{new Human("Анастасия", "Надежная", 2002, 84, new String[][]{{DayOfWeek.WEDNESDAY.name(), "танцы"},{DayOfWeek.FRIDAY.name(), "дискотека"}})};
    @Test
    public void addChild() {
        first.addChild(sun2);
        assertEquals(Arrays.toString(first.children), "[" + "Human{name = Артем, surname = Яковлев, year = 1986, iq = 87, schedule=[[THURSDAY, Спортзал]]}" + "]");
    }

    @Test
    public void deleteChild() {
        first.addChild(sun1);
        first.addChild(sun2);
        first.deleteChild(0);
        assertEquals(Arrays.toString(first.children), "[" + "Human{name = Артем, surname = Яковлев, year = 1986, iq = 87, schedule=[[THURSDAY, Спортзал]]}" + "]");
    }
    @Test
    public void deleteChild1() {
        first.addChild(sun1);
        first.addChild(sun2);
        first.deleteChild(2);
        assertEquals(Arrays.toString(first.children), "[Human{name = Глеб, surname = Яковлев, year = 1982, iq = 85, schedule=[[MONDAY, Шахматы]]}, Human{name = Артем, surname = Яковлев, year = 1986, iq = 87, schedule=[[THURSDAY, Спортзал]]}]");
    }
    @Test
    public void deleteChild2() {
        first.addChild(sun1);
        first.addChild(sun2);
        first.deleteChild(sun2);
        assertEquals(Arrays.toString(first.children), "[" + "Human{name = Глеб, surname = Яковлев, year = 1982, iq = 85, schedule=[[MONDAY, Шахматы]]}" + "]");
    }
    @Test
    public void deleteChild3() {
        first.addChild(sun1);
        first.addChild(sun2);
        first.deleteChild(daughter);
        assertEquals(Arrays.toString(first.children), "[Human{name = Глеб, surname = Яковлев, year = 1982, iq = 85, schedule=[[MONDAY, Шахматы]]}, Human{name = Артем, surname = Яковлев, year = 1986, iq = 87, schedule=[[THURSDAY, Спортзал]]}]");
    }
    @Test
    public void countFamily() {
        assertEquals(first.countFamily(), 2);
    }

    @Test
    public void testToString() {
       assertEquals(first.toString(), "Семья Яковлев\n" +
                    "mother{name = Мария, surname = Яковлева, year = 1953}\n" +
                    "father{name = Иван, surname = Яковлев, year = 1973}\n" +
                    "Child = []\n" +
                    "CAT{nickname = Мурчик, age = 5, trickLevel = 55, habits = [веселый, четкий]}");
    }
}