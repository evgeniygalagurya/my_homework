package homework7;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {
    Pet cat = new Pet(Species.CAT, "Мурчик", 5, 55, new HashSet<>(Collections.singleton("веселый и быстрый")));

    @Test
    void testToString() {
        assertEquals(cat.toString(),"CAT{nickname = Мурчик, age = 5, trickLevel = 55, habits = [веселый и быстрый]}");
    }
}