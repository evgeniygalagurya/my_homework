package homework7;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {
    Human sun1 = new Human("Глеб", "Яковлев", 1982, 85, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "отдых", DayOfWeek.WEDNESDAY, "вечеринка")));

    @Test
    void testToString() {
        assertEquals(sun1.toString(), "Human{name = Глеб, surname = Яковлев, year = 1982, iq = 85, schedule={MONDAY=отдых, WEDNESDAY=вечеринка}}");
    }
}