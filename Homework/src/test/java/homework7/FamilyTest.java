package homework7;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Pet cat = new Pet(Species.CAT, "Мурчик", 5, 55, new HashSet<>(Collections.singleton("веселый и быстрый")));
    Human sun1 = new Human("Глеб", "Яковлев", 1982, 85, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "отдых", DayOfWeek.WEDNESDAY, "вечеринка")));
    Human sun2 = new Human("Артем", "Яковлев", 1986, 87, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "баскетбол", DayOfWeek.WEDNESDAY, "вечеринка")));
    Human daughter = new Human("Анастасия", "Надежная", 2002, 84, new TreeMap<>(Map.of(DayOfWeek.MONDAY, "танцы", DayOfWeek.WEDNESDAY, "вечеринка")));
    Family first = new Family(new Human("Mariya", "Petrova", 1953, 75), new Human("Ivan", "Petrov", 1950, 80), new HashSet<Pet>(Set.of(cat)), new ArrayList<Human>());

    @Test
    void addChild() {
        first.addChild(sun1);
        assertEquals(first.toString(),"Семья Petrov\n" +
                "mother{name = Mariya, surname = Petrova, year = 1953}\n" +
                "father{name = Ivan, surname = Petrov, year = 1950}\n" +
                "Child = [Human{name = Глеб, surname = Яковлев, year = 1982, iq = 85, schedule={MONDAY=отдых, WEDNESDAY=вечеринка}}]\n" +
                "Pet = [CAT{nickname = Мурчик, age = 5, trickLevel = 55, habits = [веселый и быстрый]}]");
    }

    @Test
    void deleteChild() {
        first.addChild(sun1);
        first.deleteChild(0);
        assertEquals(first.toString(), "Семья Petrov\n" +
                "mother{name = Mariya, surname = Petrova, year = 1953}\n" +
                "father{name = Ivan, surname = Petrov, year = 1950}\n" +
                "Child = []\n" +
                "Pet = [CAT{nickname = Мурчик, age = 5, trickLevel = 55, habits = [веселый и быстрый]}]");
    }

    @Test
    void testDeleteChild() {
        first.addChild(sun1);
        first.addChild(daughter);
        first.deleteChild(daughter);
        assertEquals(first.toString(), "Семья Petrov\n" +
                "mother{name = Mariya, surname = Petrova, year = 1953}\n" +
                "father{name = Ivan, surname = Petrov, year = 1950}\n" +
                "Child = [Human{name = Глеб, surname = Яковлев, year = 1982, iq = 85, schedule={MONDAY=отдых, WEDNESDAY=вечеринка}}]\n" +
                "Pet = [CAT{nickname = Мурчик, age = 5, trickLevel = 55, habits = [веселый и быстрый]}]");
    }

    @Test
    void countFamily() {
        assertEquals(first.countFamily(), 2);

    }

    @Test
    void testToString() {
        first.addChild(daughter);
        assertEquals(first.toString(), "Семья Petrov\n" +
                "mother{name = Mariya, surname = Petrova, year = 1953}\n" +
                "father{name = Ivan, surname = Petrov, year = 1950}\n" +
                "Child = [Human{name = Анастасия, surname = Надежная, year = 2002, iq = 84, schedule={MONDAY=танцы, WEDNESDAY=вечеринка}}]\n" +
                "Pet = [CAT{nickname = Мурчик, age = 5, trickLevel = 55, habits = [веселый и быстрый]}]");
    }
}